package com.narick.model;


public class Order {
    private String inquiryNo;
    private int lineNo;
    private String customerNo;
    private String customerOrderNo;
    private String barcode;
    private int status;
    private int orderStatus;
    private String eventDate;
    private String addingDate;

    public Order() {
    }

    public Order(String inquiryNo, int lineNo, String customerNo, String customerOrderNo, String barcode, int status,
                 int orderStatus, String eventDate, String addingDate) {
        this.inquiryNo = inquiryNo;
        this.lineNo = lineNo;
        this.customerNo = customerNo;
        this.customerOrderNo = customerOrderNo;
        this.barcode = barcode;
        this.status = status;
        this.orderStatus = orderStatus;
        this.eventDate = eventDate;
        this.addingDate = addingDate;
    }

    public String getInquiryNo() {
        return inquiryNo;
    }

    public void setInquiryNo(String inquiryNo) {
        this.inquiryNo = inquiryNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerOrderNo() {
        return customerOrderNo;
    }

    public void setCustomerOrderNo(String customerOrderNo) {
        this.customerOrderNo = customerOrderNo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getAddingDate() {
        return addingDate;
    }

    public void setAddingDate(String addingDate) {
        this.addingDate = addingDate;
    }
}
