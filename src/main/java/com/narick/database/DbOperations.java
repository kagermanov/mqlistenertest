package com.narick.database;

import com.narick.model.Order;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
//import java.sql.ResultSet;

import org.apache.log4j.Logger;
import com.google.gson.Gson;

public class DbOperations {

    static Logger logger = Logger.getLogger(DbOperations.class);

    /////////////////////////
    //// INSERT ROW
    public boolean InsertJson(String row) {
        Gson json = new Gson();
        Order order = json.fromJson(row, Order.class);

        System.out.println("==================");
        System.out.println(order.getInquiryNo() + " | " + order.getLineNo() + " | " +
                order.getCustomerNo() != null ? order.getCustomerNo() : "" + " | " +
                order.getCustomerOrderNo() != null ? order.getCustomerOrderNo() : "" + " | " +
                order.getBarcode() != null ? order.getBarcode() : "" + " | " +
                order.getStatus() != null ? new Integer(order.getStatus()).toString() : 0 + " | " +
                order.getOrderStatus() != null ? new Integer(order.getOrderStatus()).toString() : "0");


        // insert object to DB
        Connection conn = null;
        //ResultSet result = null;

        //String xmlResult = "";

        try {
            conn = MSConnector.getConnection();
            System.out.println("Connected to the database. Getting data.");

            String SQL = "insert into [Order Status] ([Inquiry No_], [Line No_], " +
                    "[Customer No_], [Customer Order No_], [BarCode], [Status], [Order Status], [Event Date], [Adding Date]) " +
                    "values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement stmt = conn.prepareStatement(SQL);

            stmt.setString(1, order.getInquiryNo());
            stmt.setInt(2, order.getLineNo());
            stmt.setString(3, order.getCustomerNo() != null ? order.getCustomerNo() : "");
            stmt.setString(4, order.getCustomerOrderNo() != null ? order.getCustomerOrderNo() : "");
            stmt.setString(5, order.getBarcode() != null ? order.getBarcode() : "");
            stmt.setInt(6, order.getStatus());
            stmt.setInt(7, order.getOrderStatus());
            stmt.setString(8, order.getEventDate());
            stmt.setString(9, getCurrentDatetime().toString());

            stmt.executeUpdate();


        } catch (Exception e) {
            e.printStackTrace();
            logger.error("<<<<" + e.getMessage() + ">>>>");
            System.out.println("<<< ERROR: " + e.getMessage() + " >>>");
            return false;
        } finally {
            MSConnector.connectionClose();
        }

        return true;
    }

    private java.sql.Date getCurrentDatetime() {
        java.util.Date today = new java.util.Date();
        return new java.sql.Date(today.getTime());
    }
}
