package com.narick;

import com.narick.database.DbOperations;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import org.apache.log4j.Logger;

import javax.jms.*;
import java.util.EmptyStackException;

public class Consumer {

    static Logger logger = Logger.getLogger(Consumer.class);
    static String queueName = "Testqueue8";

    static String topicName = "TestTopic_1";

    public Consumer() throws JMSException {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        //connectionFactory.setBrokerURL("tcp://localhost:61616");
        connectionFactory.setBrokerURL(ActiveMQConnection.DEFAULT_BROKER_URL);
        connectionFactory.setUserName("karaf");
        connectionFactory.setPassword("karaf");

        Connection connection = connectionFactory.createConnection();
        connection.start();

        //Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
        //MessageConsumer consumer = session.createConsumer(session.createQueue(queueName));
        MessageConsumer consumer = session.createConsumer(session.createTopic(topicName));
        consumer.setMessageListener(new HelloMessageListener());

    }

    private static class HelloMessageListener implements MessageListener {

        @Override
        public void onMessage(Message message) {
            TextMessage textMessage = (TextMessage) message;
            try {
                System.out.println(textMessage.getText());
                DbOperations operations = new DbOperations();
                if (operations.InsertJson(textMessage.getText())) {
                    textMessage.acknowledge();
                    System.out.println("received message: " + textMessage.getText());
                    logger.info("<<<<" + "Consumer " + Thread.currentThread().getName() + " received message: " + textMessage.getText() + ">>>>");
                }
            } catch (JMSException e) {
                e.printStackTrace();
                logger.error("<<<<" + e.getMessage() + ">>>>");
            }
        }

    }
}
